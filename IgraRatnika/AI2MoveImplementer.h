#pragma once
#include "AbstractMoveImplementer.h"

class IMyGame;

class AI2MoveImplementer : public AbstractMoveImplementer {
public:
	AI2MoveImplementer(IMyGame* iMyGame); //check why AI2MoveImplementer(IMyGame* iMyGame) : AbstractMoveImplementer(iMyGame){} is not working
	~AI2MoveImplementer() {};
	void attack(int teamId);
};

