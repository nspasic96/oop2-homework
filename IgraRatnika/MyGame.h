#pragma once
#include "IMyGame.h"
class MyGame : public IMyGame{
protected:
	virtual void updateAfterFight(Player * loser) override;
public:
	MyGame() : IMyGame() {};
	~MyGame() {};

	// Inherited via IMyGame
	virtual Player * getNextPlayer() override;
	virtual vector<int> getAlivePlayerIds(int exceptTeamId) override;
	virtual void attack(int attackerId, int attackersWarriorId, int defenderId, int defendersWarriorId) override;
	virtual void createGame(int numOfHumanPlayers, int numOfBotPlayers,IMyGame* iMyGame) override;
	virtual map<int, IWarrior*> getAliveWarriorsByPlayerId(int idTeamToAttack) override;
	virtual void play() override;
	virtual bool fight(IWarrior *, IWarrior *) override;
	virtual void printGameState() override;
};

