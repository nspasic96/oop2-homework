#include "WarriorDecorator.h"
#include "Warrior.h"

void WarriorDecorator::setDecorates(IWarrior * decorates){
	this->decorates = decorates;
}

double WarriorDecorator::getIntelligence(){
	return decorates->getIntelligence();
}

double WarriorDecorator::getSpeed(){
	return decorates->getSpeed();
}

double WarriorDecorator::getStrength(){
	return decorates->getStrength();
}

void WarriorDecorator::setHairColor(double toSet){
	Warrior* w = dynamic_cast<Warrior*>(decorates);
	if (w != nullptr) {
		w->setHairColor(toSet);
	} else {
		decorates->setHairColor(toSet);
	}
}

void WarriorDecorator::setSkinColor(double toSet){
	Warrior* w = dynamic_cast<Warrior*>(decorates);
	if (w != nullptr) {
		w->setSkinColor(toSet);
	}
	else {
		decorates->setSkinColor(toSet);
	}
}

void WarriorDecorator::setSpeed(double toSet)
{
	Warrior* w = dynamic_cast<Warrior*>(decorates);
	if (w != nullptr) {
		w->setSpeed(toSet);
	}
	else {
		decorates->setSpeed(toSet);
	}
}

void WarriorDecorator::setIntelligence(double toSet)
{
	Warrior* w = dynamic_cast<Warrior*>(decorates);
	if (w != nullptr) {
		w->setIntelligence(toSet);
	}
	else {
		decorates->setIntelligence(toSet);
	}
}

void WarriorDecorator::setStrength(double toSet)
{
	Warrior* w = dynamic_cast<Warrior*>(decorates);
	if (w != nullptr) {
		w->setStrength(toSet);
	}
	else {
		decorates->setStrength(toSet);
	}
}

void WarriorDecorator::setTiredness(double toSet)
{
	Warrior* w = dynamic_cast<Warrior*>(decorates);
	if (w != nullptr) {
		w->setHairColor(toSet);
	}
	else {
		decorates->setHairColor(toSet);
	}
}

double WarriorDecorator::getHairColor(){
	return decorates->getHairColor();
}

double WarriorDecorator::getSkinColor(){
	return decorates->getSkinColor();
}

double WarriorDecorator::getTiredness(){
	return decorates->getTiredness();
}
