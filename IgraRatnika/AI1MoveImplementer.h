#pragma once
#include "AbstractMoveImplementer.h"

class IMyGame;

class AI1MoveImplementer : public AbstractMoveImplementer{
public:
	AI1MoveImplementer(IMyGame* iMyGame);
	~AI1MoveImplementer() {};
	void attack(int teamId);
};

