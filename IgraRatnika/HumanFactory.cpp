#include "HumanFactory.h"

IWarrior * HumanFactory::makeWarrior(double skillPoints){
	double d;
	IWarrior* iWarrior = 0;
	cout << "Making new warrior with " << skillPoints << " slill points"<<endl;
	cout << "Min possible skill points is " << skillMin << " and max is" << skillMax << endl;
	cout << "Insert hair color";
	cin >> d;
	warriorBuilder = warriorBuilder->setHairColor(d);
	cout << endl;
	cout << "Insert skin color";
	cin >> d;
	warriorBuilder = warriorBuilder->setSkinColor(d);
	cout << endl;
	try {
		cout << "Insert intelligence(skill)";
		cin >> d;
		warriorBuilder = warriorBuilder->setIntelligence(d);
		cout << endl;
		cout << "Insert speed(skill)";
		cin >> d;
		warriorBuilder = warriorBuilder->setSpeed(d);
		cout << endl;
		cout << "Insert strength(skill)";
		cin >> d;
		warriorBuilder = warriorBuilder->setStrength(d);
		cout << endl;	
		iWarrior = warriorBuilder->build(skillPoints);
	}
	catch (SkillPointsException& e) {
		cout<< e.what();
		cout << "Making warrior again" << endl;
		makeWarrior(skillPoints);
	}
	return iWarrior;
}

map<int, IWarrior*>* HumanFactory::makeWarriors(int numberOfWarriors, double skillPoints){
	cout << "Human is making team";
	map<int, IWarrior*>* team = new map<int,IWarrior*>();
	for (int i = 1; i <= numberOfWarriors; i++) {
		team->insert(pair<int,IWarrior*>(i, makeWarrior(skillPoints)));//need to check this, some error posible here because pair is created on stack?
	}
	return team;
}
