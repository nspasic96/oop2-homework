#include "HumanMoveImplementer.h"

void HumanMoveImplementer::attack(int teamId) {
	int idTeamToAttack, idWarriorToAttack, warriorId,i=0;
	cout << "Your allive warriors are:" << endl;
	map<int, IWarrior*> b = iMyGame->getAliveWarriorsByPlayerId(teamId);
	for (map<int, IWarrior*>::iterator it = b.begin(); it != b.end(); it++) {
		i++;
		cout << "  " << i << ". id=" << it->first << endl;
		cout << "    " << it->second;
	}
	cin >> warriorId;
	vector<int> a = iMyGame->getAlivePlayerIds(teamId);
	cout << "Alive teams:" << endl;
	i = 0;
	for (vector<int>::iterator it = a.begin(); it != a.end(); it++) {
		i++;
		cout << "  " << i << ". Team with id=" << *it << endl;
	}
	cout << "Insert team id to attack";
	cin >> idTeamToAttack;
	map<int, IWarrior*> c = iMyGame->getAliveWarriorsByPlayerId(idTeamToAttack);
	cout << "Allive warriors in team: ";
	i = 0;
	for (map<int, IWarrior*>::iterator it = c.begin(); it != c.end(); it++) {
		i++;
		cout << "  " << i << ". id=" << it->first << endl;
		cout << "    " << it->second;
	}
	cout << "Insert warrior id to attack";
	cin >> idWarriorToAttack;
	try {
		iMyGame->attack(teamId, warriorId, idTeamToAttack, idWarriorToAttack);
	}
	catch (NoTeamWithGivenIdException& e1) {
		cout << e1.what();
		cout << "Try to attack again" << endl;
		attack(teamId);
	}
	catch (NoWarriorWithGivenIdInTeamWithGivenId& e2) {
		cout << e2.what();
		cout << "Try to attack again" << endl;
		attack(teamId);
	}
}

HumanMoveImplementer::HumanMoveImplementer(IMyGame* iMyGame) : AbstractMoveImplementer(iMyGame) {};
