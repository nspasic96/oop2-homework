#pragma once
#include "TeamWarriorFactory.h"
class AI1Factory : public TeamWarriorFactory{
protected:
	AI1Factory() : TeamWarriorFactory() {};
	~AI1Factory();
public:
	static AI1Factory* getInstance() {
		static AI1Factory ai1Factory;
		return &ai1Factory;
	}

	// Inherited via TeamWarriorFactory
	virtual IWarrior * makeWarrior(double skillPoints) override;
	virtual map<int, IWarrior*>* makeWarriors(int numberOfWarriors, double skillPoints) override;
};

