#pragma once
#include "AbstractMoveImplementer.h"

class IMyGame;

class HumanMoveImplementer : public AbstractMoveImplementer {
public:
	HumanMoveImplementer(IMyGame* iMyGame);
	~HumanMoveImplementer() {};
	void attack(int attackerId);
};

