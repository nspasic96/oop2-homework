#include "AI2MoveImplementer.h"

using namespace std;

//this move implementor gets random team id and attacks random warrior in that team with his random chosen warrior
void AI2MoveImplementer::attack(int teamId) {
	int idTeamToAttack, idWarriorToAttack, warriorId,randomInt=0;
	map<int, IWarrior*> b = iMyGame->getAliveWarriorsByPlayerId(teamId);
	randomInt = (rand() % b.size());
	map<int, IWarrior*>::iterator randomElem = b.begin();
	advance(randomElem, randomInt);
	warriorId = randomElem->first;
	vector<int> a = iMyGame->getAlivePlayerIds(teamId);
	randomInt = (rand() % a.size());
	vector<int>::iterator randomElem2 = a.begin();
	advance(randomElem2, randomInt);
	idTeamToAttack = *randomElem2;
	map<int, IWarrior*> d = iMyGame->getAliveWarriorsByPlayerId(idTeamToAttack);
	randomInt = (rand() % d.size());
	map<int, IWarrior*>::iterator randomElem3 = d.begin();
	advance(randomElem3, randomInt);
	idWarriorToAttack = randomElem3->first;
	iMyGame->attack(teamId, warriorId, idTeamToAttack, idWarriorToAttack);
}

AI2MoveImplementer::AI2MoveImplementer(IMyGame* iMyGame) : AbstractMoveImplementer(iMyGame) {};

