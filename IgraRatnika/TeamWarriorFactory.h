#pragma once
#include "Constants.h"
#include "Warrior.h"
#include "WarriorBuilder.h"
#include <map>
#include <iostream>

using namespace std;

class TeamWarriorFactory {
protected:
	TeamWarriorFactory():warriorBuilder(new WarriorBuilder()) {};
	virtual ~TeamWarriorFactory() { delete warriorBuilder; };
	WarriorBuilder* warriorBuilder;
public:
	virtual IWarrior* makeWarrior(double skillPoints)=0;
	virtual map<int, IWarrior*>* makeWarriors(int numberOfWarriors, double skillPoints)=0;
};
