#pragma once
#include "WarriorDecorator.h"
class SuperHeroDecorator : WarriorDecorator {
private:
	heroSpecialSkill hSS;
	double additionalSkillPoints;
public:
	SuperHeroDecorator(IWarrior* iWarrior, heroSpecialSkill hSS, double additionalSkillPoints) : WarriorDecorator(iWarrior), 
		hSS(hSS), additionalSkillPoints(additionalSkillPoints) {};
	~SuperHeroDecorator() {};

	virtual double getStrength() override;
	virtual double getSpeed() override;
	virtual double getIntelligence() override;
	
};

