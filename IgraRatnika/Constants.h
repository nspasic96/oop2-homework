#pragma once

const double maxSkillPointsForWarrior = 100;
const int numberOfWarriors = 10;
const int turnsForNewWarrior = 5;
const int maxNumberOfPlayers = 6;
const int heroSkillAdditionalSkillPoints = 10;
enum playerType { AI1, AI2, HUMAN };
const double skillMin = 0;
const double skillMax = 100;
enum heroSpecialSkill { SPEED, STRENGTH, INTELLIGENCE };