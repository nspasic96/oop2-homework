#pragma once
#include "IWarrior.h"
class Warrior : public IWarrior{
public:
	Warrior() : IWarrior(), tiredness(0) {};
	~Warrior() {};

	// Inherited via IWarrior
	virtual void setHairColor(double) override;
	virtual void setSkinColor(double) override;
	virtual void setSpeed(double) override;
	virtual void setIntelligence(double) override;
	virtual void setStrength(double) override;
	virtual void setTiredness(double) override;
	virtual double getHairColor() override;
	virtual double getSkinColor() override;
	virtual double getSpeed() override;
	virtual double getIntelligence() override;
	virtual double getStrength() override;
	virtual double getTiredness() override;
private:
	/*
		Characteristics	that don't affect to result of fight.
	*/
	double hairColor;
	double skinColor;
	
	/*
	Characteristics	that do affect to result of fight and use skill points.
	*/
	double speed;
	double intelligence;
	double strength;
	
	/*
	Characteristics	that do affect to result of fight and don't use skill points.
	*/
	double tiredness;
	
};

