#include "WarriorBuilder.h"

WarriorBuilder::~WarriorBuilder() { delete curWarrior; }

WarriorBuilder * WarriorBuilder::setHairColor(double toSet){
	curWarrior->setHairColor(toSet);
	return this;
}

WarriorBuilder * WarriorBuilder::setSkinColor(double toSet){
	curWarrior->setSkinColor(toSet);
	return this;
}

WarriorBuilder * WarriorBuilder::setSpeed(double toSet){
	if (toSet > skillMax || toSet < skillMin) throw new SkillPointsException();
	curWarrior->setSpeed(toSet);
	return this;
}

WarriorBuilder * WarriorBuilder::setIntelligence(double toSet){
	if (toSet > skillMax || toSet < skillMin) throw new SkillPointsException();
	curWarrior->setIntelligence(toSet);
	return this;
}

WarriorBuilder * WarriorBuilder::setStrength(double toSet){
	if (toSet > skillMax || toSet < skillMin) throw new SkillPointsException();
	curWarrior->setStrength(toSet);
	return this;
}

IWarrior * WarriorBuilder::build(double maxSkillPoints){
	if (curWarrior->getSpeed() + curWarrior->getIntelligence() + curWarrior->getStrength() > maxSkillPoints) throw new SkillPointsException();
	IWarrior* toRet = curWarrior;
	curWarrior = new Warrior();
	return toRet;
}
