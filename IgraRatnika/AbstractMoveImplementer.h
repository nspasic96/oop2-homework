#pragma once
#include <exception>
#include <vector>
#include "IMyGame.h"
#include<iostream>

using namespace std;

class IMyGame;

class AbstractMoveImplementer {
protected:
	IMyGame* iMyGame;
public:
	AbstractMoveImplementer(IMyGame* myGame) : iMyGame(myGame) {};
	virtual ~AbstractMoveImplementer() {};
	virtual void attack(int teamId) = 0;
};

class NoWarriorWithGivenIdInTeamWithGivenId : public exception {

};

class CantAttackYourself : public exception {
	virtual const char* what() const throw() {
		char* msg = "Cannot attack yourself";
		return msg;
	}
};
