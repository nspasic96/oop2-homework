#pragma once
#include "Constants.h"
#include <exception>
#include <string>
#include <iostream>

using namespace std;

class SkillPointsException : public exception {
public:
	virtual const char* what() const throw() {
		char* msg = "You are trying to set skill points to warrior outside [";
		strcat(msg, to_string(skillMin).c_str());
		strcat(msg, ",");
		strcat(msg, to_string(skillMax).c_str());
		strcat(msg, " range or sum of given warrior's skill points is larger then ");
		strcat(msg, to_string(maxSkillPointsForWarrior).c_str());
		return msg;
	}
};

class IWarrior{

public:
	IWarrior() {};
	virtual ~IWarrior() {};
	virtual double getHairColor()=0;
	virtual double getSkinColor() = 0;
	virtual double getSpeed() = 0;
	virtual double getIntelligence() = 0;
	virtual double getStrength() = 0;
	virtual double getTiredness() = 0;
	virtual void setHairColor(double) = 0;
	virtual void setSkinColor(double) = 0;
	virtual void setSpeed(double) = 0;
	virtual void setIntelligence(double) = 0;
	virtual void setStrength(double) = 0;
	virtual void setTiredness(double) = 0;
	friend ostream& operator<<(ostream& os, IWarrior* iw);
};
ostream& operator<<(ostream& os, IWarrior* iw) {
	cout << "[Hair color, Skin color, Speed, Intelligence, Strength, Tiredness] = [" << iw->getHairColor() << ", "
		<< iw->getSkinColor() << ", " << iw->getSpeed() << ", " << iw->getIntelligence() << ", " 
		<< iw->getStrength() << ", " << iw->getTiredness();
	return os;
}
