#pragma once
#include "IWarrior.h"
#include "Constants.h"
class WarriorDecorator : public IWarrior{
protected:
	IWarrior* decorates;
public:
	WarriorDecorator(IWarrior* dec) : IWarrior(), decorates(dec){};
	~WarriorDecorator() { delete decorates; };
	void setDecorates(IWarrior*);

	// Inherited via IWarrior
	virtual double getHairColor() override;
	virtual double getSkinColor() override;
	virtual double getTiredness() override;
	virtual double getIntelligence() override;
	virtual double getSpeed() override;
	virtual double getStrength() override;
	virtual void setHairColor(double) override;
	virtual void setSkinColor(double) override;
	virtual void setSpeed(double) override;
	virtual void setIntelligence(double) override;
	virtual void setStrength(double) override;
	virtual void setTiredness(double) override;
};

