#pragma once
#include "Warrior.h"
class WarriorBuilder{
private: 
	Warrior* curWarrior;
public:
	WarriorBuilder() :curWarrior(new Warrior()) {};
	~WarriorBuilder();
	WarriorBuilder* setHairColor(double);
	WarriorBuilder* setSkinColor(double);
	WarriorBuilder* setSpeed(double);
	WarriorBuilder* setIntelligence(double);
	WarriorBuilder* setStrength(double);
	WarriorBuilder* setTiredness(double);
	IWarrior* build(double maxSkillPoints);


};

