#include "MyGame.h"


Player * MyGame::getNextPlayer(){
	map<int, Player*>::iterator it = alivePlayers.find(playNext);
	advance(it, 1);
	if (it == alivePlayers.end()) {
		playNext = alivePlayers.begin()->first;
	} else {
		playNext = it->first;
	}
	return it->second;
}

vector<int> MyGame::getAlivePlayerIds(int exceptTeamId){
	vector<int> a = vector<int>();
	for (map<int, Player*>::iterator it = alivePlayers.begin(); it != alivePlayers.end(); it++) {
		if (it->first != exceptTeamId) {
			a.push_back(it->first);
		}
	}
	return a;
}

void MyGame::attack(int attackerId, int attackersWarriorId, int defenderId, int defendersWarriorId){
	Player* attacker = alivePlayers.find(attackerId)->second;	
	IWarrior* aw = attacker->findWarrior(attackersWarriorId)->second;
	Player* defender = alivePlayers.find(defenderId)->second;
	IWarrior* df = defender->findWarrior(defendersWarriorId)->second;
	bool atWon = fight(aw, df);
	if (atWon) {
		updateAfterFight(defender);
	} else {
		updateAfterFight(attacker);
	}
}

void MyGame::updateAfterFight(Player* loser) {
	int loserId = loser->getPlayerId();
	loser->removeWarrior(loserId);
	//player died, need to update firstPlayerId and playNext
	if (loser->countWarriors() == 0) {
		if (loserId == playNext) {
			getNextPlayer();
		}
		if (loserId == firstPlayerId) {
			//if this is not last move
			if (numberOfPlayersInGame() > 2) {
				map<int, Player*>::iterator x = alivePlayers.find(loserId);
				//loser was first in map of alive players
				if (x != alivePlayers.begin()) {
					advance(x, -1);
				}
				else {
					advance(x, 1);
				}
				firstPlayerId = x->second->getPlayerId();
			}
		}
		alivePlayers.erase(alivePlayers.find(loserId));
		cout << "Player with id=" << loserId << " has been destroyed";
		delete loser;
	}
}

void MyGame::createGame(int numOfHumanPlayers, int numOfBotPlayers, IMyGame* iMyGame){
	if(numOfHumanPlayers + numOfBotPlayers > maxNumberOfPlayers || numOfHumanPlayers + numOfBotPlayers<2 || numOfBotPlayers<0 || numOfHumanPlayers<0) throw new NumberOfPlayersException();
	firstPlayerId = 1;
	playNext = 1;
	for (int i = 0; i < numOfHumanPlayers; i++) {
		alivePlayers.insert(pair<int,Player*>(i + 1, new Player(new HumanMoveImplementer(iMyGame),HumanFactory::getInstance())));
	}
	for (int i = 0; i < numOfBotPlayers; i++) {
		alivePlayers.insert(pair<int, Player*>
			(numOfHumanPlayers + i + 1, new Player(i % 2 ? new AI1MoveImplementer(iMyGame) : new AI1MoveImplementer(iMyGame)
				, i % 2 ? (TeamWarriorFactory*)AI1Factory::getInstance() : AI2Factory::getInstance()))//this needs to be checked
			);
	}
}

map<int, IWarrior*> MyGame::getAliveWarriorsByPlayerId(int idPlayer){
	map<int, IWarrior*> res = map<int, IWarrior*>();
	map<int, Player*>::iterator a = alivePlayers.find(idPlayer);
	if (a == alivePlayers.end()) {
		throw new NoTeamWithGivenIdException(idPlayer);
	}
	return a->second->getWarriors();
}

void MyGame::play(){
	while (numberOfPlayersInGame() > 1) {
		printGameState();
		Player* next = getNextPlayer();
		//every {Constants::turnsForNewWarrior} turns, everyone get new Warrior
		if (next->getPlayerId() == firstPlayerId) {
			cout << "New turn started" << endl;
			turn++;
		}
		if ((turn % turnsForNewWarrior) == 0) {
			cout << "Every " << turnsForNewWarrior << " turns, all warriors get new warrior" << endl;
			for (map<int, Player*>::iterator it = alivePlayers.begin(); it != alivePlayers.end(); it++) {
				it->second->makeNewWarrior();
			}
		}
		next->play();
	}
	cout << "Winner is player with id=" << alivePlayers.begin()->second->getPlayerId();
	
}

bool MyGame::fight(IWarrior *at, IWarrior *def){
	//here goes logic for fight, true for attacker won, false for attacker lose
	double a = at->getIntelligence()*at->getSpeed()*at->getStrength() - at->getTiredness();
	double b = def->getIntelligence()*def->getSpeed()*def->getStrength() - def->getTiredness();
	//winner gets more tired after fight
	at->setTiredness(at->getTiredness() + at->getIntelligence() + at->getSpeed() + at->getStrength());
	def->setTiredness(def->getTiredness() + def->getIntelligence() + def->getSpeed() + def->getStrength());
	return (a >= b);
}

void MyGame::printGameState(){
	cout<< "************************************************" << endl;
	cout << "CURRENT GAME STATE IS:" << endl;
	for (map<int, Player*>::iterator it = alivePlayers.begin(); it != alivePlayers.end();it++) {
		cout << *(it->second);
	}
	cout << "************************************************" << endl;
}
