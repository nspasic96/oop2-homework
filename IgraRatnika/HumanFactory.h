#pragma once
#include "TeamWarriorFactory.h"
class HumanFactory : public TeamWarriorFactory{
protected:
	HumanFactory() : TeamWarriorFactory() {};
	~HumanFactory() {};
public:
	static HumanFactory* getInstance() {
		static HumanFactory humanFactory;
		return &humanFactory;
	}

	// Inherited via TeamWarriorFactory
	virtual IWarrior * makeWarrior(double skillPoints) override;

	virtual map<int, IWarrior*>* makeWarriors(int numberOfWarriors, double skillPoints) override;

};

