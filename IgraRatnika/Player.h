#pragma once
#include "IWarrior.h"
#include "AbstractMoveImplementer.h"
#include "TeamWarriorFactory.h"
#include <exception>
#include <string>
#include "Constants.h"
#include "WarriorDecorator.h"

class AbstractMoveImplementer;

using namespace std;

class NoWarriorWithIdInTeamException : public exception {
private:
	int teamId,warriorId;
public:
	NoWarriorWithIdInTeamException(int teamId, int warriorId):teamId(teamId),warriorId(warriorId) {};
	virtual const char* what(int teamId, int warriorId) const throw(){
		char* msg = new char[100];
		strcat(msg, "No warrior with id ");
		strcat(msg, to_string(warriorId).c_str());
		strcat(msg, " in team with id ");
		strcat(msg, to_string(teamId).c_str());
		return msg;
	}
};
class Player {
private:
	static int nextPlayerId();
	int playerId;
	int lastWarriorId;
	map<int, IWarrior*>* warriors;
	AbstractMoveImplementer *mi;
	TeamWarriorFactory* twf;
public:
	Player(AbstractMoveImplementer*, TeamWarriorFactory*);
	~Player();
	void play();
	void insertNewWarrior(IWarrior*);
	void decorateWarrior(int id, WarriorDecorator* wd);
	void removeWarrior(int id);
	map<int, IWarrior*>::iterator findWarrior(int id);	
	map<int, IWarrior*> getWarriors() { return *warriors; };
	int getPlayerId() { return playerId; }
	int countWarriors() { return warriors->size(); };
	void makeNewWarrior();
	friend ostream& operator<<(ostream& o, const Player& p);
};

