#pragma once
#include "Constants.h"
#include "IWarrior.h"
#include "Player.h"
#include "AI1MoveImplementer.h"
#include "AI2MoveImplementer.h"
#include "HumanFactory.h"
#include "HumanMoveImplementer.h"
#include "AI1Factory.h"
#include "AI2Factory.h"
#include "TeamWarriorFactory.h"

using namespace std;

class Player;

class NumberOfPlayersException : public exception {
	virtual const char* what() const throw() {
		char* msg = new char[100];
		strcat(msg, "Negative number of human or bot players is not allowed. Min number of players in game is 2 and max number of players in game is ");
		strcat(msg, to_string(maxNumberOfPlayers).c_str());
		return msg;
	}
};
class NoTeamWithGivenIdException : public exception {
private:
	int teamId;
public:
	NoTeamWithGivenIdException(int teamId) :teamId(teamId) {};
	virtual const char* what() const throw() {
		char* msg = new char[100];
		strcat(msg, "Team with id=");
		strcat(msg, to_string(teamId).c_str());
		strcat(msg, " not in game.");
		return msg;
	}
};

class IMyGame{
protected:
	map<int,Player*> alivePlayers;
	int numberOfPlayersInGame() { return alivePlayers.size(); };
	IMyGame() : turn(0), alivePlayers(map<int,Player*>()){};
	virtual ~IMyGame() { 
		for (map<int,Player*>::iterator it = alivePlayers.begin(); it != alivePlayers.end(); it++) {
			delete it->second;
		}
		alivePlayers.clear();
	};
	int turn;
	virtual bool fight(IWarrior*,IWarrior*) = 0; //maybe template pattern because of this?
	//player that makes first move
	int firstPlayerId;
	virtual void updateAfterFight(Player*) = 0;
	//id of player to play next
	int playNext;
public:
	virtual Player* getNextPlayer() = 0;
	virtual vector<int> getAlivePlayerIds(int exceptTeamId) = 0;
	virtual void attack(int attackerId, int attackersWarriorId, int defenderId, int defendersWarriorId) = 0;
	virtual void createGame(int numOfHumanPlayers, int numOfBotPlayers, IMyGame* iMyGame) = 0;
	virtual map<int, IWarrior*> getAliveWarriorsByPlayerId(int idTeam) = 0;
	virtual void play() = 0;
	virtual void printGameState() = 0;

};

