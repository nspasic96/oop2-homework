#include "Warrior.h"

void Warrior::setHairColor(double toSet){
	hairColor = toSet;
}

void Warrior::setSkinColor(double toSet){
	skinColor = toSet;
}

void Warrior::setSpeed(double toSet){
	speed = toSet;
}

void Warrior::setIntelligence(double toSet){
	intelligence = toSet;
}

void Warrior::setStrength(double toSet){
	strength = toSet;
}

void Warrior::setTiredness(double toSet){
	tiredness = toSet;
}

double Warrior::getHairColor(){
	return hairColor;
}

double Warrior::getSkinColor(){
	return skinColor;
}

double Warrior::getSpeed(){
	return speed;
}

double Warrior::getIntelligence(){
	return intelligence;
}

double Warrior::getStrength(){
	return strength;
}

double Warrior::getTiredness(){
	return tiredness;
}




