#include "AI2Factory.h"

//this factory makes warrior with max intelligence, then max speed and then max strength
IWarrior * AI2Factory::makeWarrior(double skillPoints){
	IWarrior* iWarrior = 0;
	cout << "AI2 Factory is making warrior with " << skillPoints << " slill points" << endl;
	warriorBuilder = warriorBuilder->setHairColor(25);
	warriorBuilder = warriorBuilder->setSkinColor(150);
	try {
		double skillPointsLeft = skillPoints > maxSkillPointsForWarrior ? maxSkillPointsForWarrior : skillPoints;

		double intSkill = skillPointsLeft > skillMax ? skillMax : skillPointsLeft;
		skillPointsLeft -= intSkill;
		warriorBuilder = warriorBuilder->setIntelligence(intSkill);

		double speedSkill = skillPointsLeft > skillMax ? skillMax : skillPointsLeft;
		warriorBuilder = warriorBuilder->setSpeed(speedSkill);
		skillPointsLeft -= speedSkill;

		double strengthSkill = skillPointsLeft > skillMax ? skillMax : skillPointsLeft;
		warriorBuilder = warriorBuilder->setStrength(strengthSkill);
		skillPointsLeft -= strengthSkill;

		iWarrior = warriorBuilder->build(skillPoints);
	}
	catch (SkillPointsException& e) {
		cout << e.what();
		cout << "Making warrior again" << endl;
		makeWarrior(skillPoints);
	}
	return iWarrior;
}

map<int, IWarrior*>* AI2Factory::makeWarriors(int numberOfWarriors, double skillPoints){
	cout << "AI2 factory is making team" << endl;
	map<int, IWarrior*>* team = new map<int, IWarrior*>();
	for (int i = 1; i <= numberOfWarriors; i++) {
		team->insert(pair<int, IWarrior*>(i, makeWarrior(skillPoints)));
	}
	return team;
}
