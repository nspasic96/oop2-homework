#include "AI1MoveImplementer.h"

//this move implementor gets first team id and attacks first warrior in that team with his first warrior
void AI1MoveImplementer::attack(int teamId) {
	int idTeamToAttack, idWarriorToAttack, warriorId;
	map<int, IWarrior*> b = iMyGame->getAliveWarriorsByPlayerId(teamId);
	warriorId = b.begin()->first;
	vector<int> a = iMyGame->getAlivePlayerIds(teamId);
	idTeamToAttack = *(a.begin());
	b = iMyGame->getAliveWarriorsByPlayerId(idTeamToAttack);
	idWarriorToAttack = b.begin()->first;
	iMyGame->attack(teamId, warriorId, idTeamToAttack, idWarriorToAttack);
}

AI1MoveImplementer::AI1MoveImplementer(IMyGame* iMyGame) : AbstractMoveImplementer(iMyGame) {};

