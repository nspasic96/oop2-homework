#include "AI2Factory.h"

//this factory makes warrior with max strength, then max speed and then max intelligence
IWarrior * AI2Factory::makeWarrior(double skillPoints) {
	IWarrior* iWarrior = 0;
	cout << "AI1 Factory is making warrior with " << skillPoints << " slill points" << endl;
	warriorBuilder = warriorBuilder->setHairColor(10);
	warriorBuilder = warriorBuilder->setSkinColor(20);
	try {
		double skillPointsLeft = skillPoints > maxSkillPointsForWarrior ? maxSkillPointsForWarrior : skillPoints;

		double strengthSkill = skillPointsLeft > skillMax ? skillMax : skillPointsLeft;
		warriorBuilder = warriorBuilder->setStrength(strengthSkill);
		skillPointsLeft -= strengthSkill;

		double speedSkill = skillPointsLeft > skillMax ? skillMax : skillPointsLeft;
		warriorBuilder = warriorBuilder->setSpeed(speedSkill);
		skillPointsLeft -= speedSkill;
		
		double intSkill = skillPointsLeft > skillMax ? skillMax : skillPointsLeft;
		skillPointsLeft -= intSkill;
		warriorBuilder = warriorBuilder->setIntelligence(intSkill);

		iWarrior = warriorBuilder->build(skillPoints);
	}
	catch (SkillPointsException& e) {
		cout << e.what();
		cout << "Making warrior again" << endl;
		makeWarrior(skillPoints);
	}
	return iWarrior;
}

map<int, IWarrior*>* AI2Factory::makeWarriors(int numberOfWarriors, double skillPoints) {
	cout << "AI1 factory is making team" << endl;
	map<int, IWarrior*>* team = new map<int, IWarrior*>();
	for (int i = 1; i <= numberOfWarriors; i++) {
		team->insert(pair<int, IWarrior*>(i, makeWarrior(skillPoints)));
	}
	return team;
}
