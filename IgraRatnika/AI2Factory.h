#pragma once
#include "TeamWarriorFactory.h"
class AI2Factory : public TeamWarriorFactory {
protected:
	AI2Factory() : TeamWarriorFactory() {};
	~AI2Factory();
public:
	static AI2Factory* getInstance() {
		static AI2Factory ai2Factory;
		return &ai2Factory;
	}

	// Inherited via TeamWarriorFactory
	virtual IWarrior * makeWarrior(double skillPoints) override;
	virtual map<int, IWarrior*>* makeWarriors(int numberOfWarriors, double skillPoints) override;
};

