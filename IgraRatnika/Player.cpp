#include "Player.h"

int Player::nextPlayerId(){
	static int nextPlayerId = 0;
	return nextPlayerId++;
}

void Player::removeWarrior(int id){
	map<int, IWarrior*>::iterator a = findWarrior(id);
	delete a->second;
	warriors->erase(a);
}

map<int, IWarrior*>::iterator Player::findWarrior(int id){
	map<int, IWarrior*>::iterator res = warriors->find(id);
	if (res == warriors->end()) {
		throw NoWarriorWithIdInTeamException(playerId,id);
	}
	return res;
}

Player::Player(AbstractMoveImplementer *mi, TeamWarriorFactory *twf) : mi(mi), twf(twf){
	lastWarriorId = numberOfWarriors;
	playerId = nextPlayerId();
	warriors = twf->makeWarriors(numberOfWarriors,maxSkillPointsForWarrior);
}

Player::~Player(){
	for (map<int, IWarrior*>::iterator it = warriors->begin(); it != warriors->end(); it++) {
		delete it->second;
		it->second = 0;
	}
	warriors->clear();
	delete warriors;//maybe not?
	delete mi;
}

void Player::play(){
	mi->attack(playerId);
}

void Player::makeNewWarrior() {
	insertNewWarrior(twf->makeWarrior(maxSkillPointsForWarrior));
}

void Player::insertNewWarrior(IWarrior* newOne){
	warriors->insert(pair<int, IWarrior*>(++lastWarriorId,newOne));
}

void Player::decorateWarrior(int id, WarriorDecorator * wd){
	map<int, IWarrior*>::iterator it = findWarrior(id);
	wd->setDecorates(it->second);
	it->second = wd;
}

ostream& operator<<(ostream& os, const Player& p) {
	cout << "Player with id " << p.playerId << " has " << (p.warriors)->size() << " warriors:" << endl;
	map<int, IWarrior*> a = *p.warriors;
	int i = 0;
	for (map<int, IWarrior*>::iterator it = a.begin(); it != a.end(); it++) {
		i++;
		cout<<"  "<< i << ". id=" << it->first << " "<< (it->second) << endl;
	}
	return os;
}
